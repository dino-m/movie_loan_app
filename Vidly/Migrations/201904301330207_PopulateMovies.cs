namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMovies : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Movies ON");
            Sql("INSERT INTO Movies (Id, Name, ReleaseDate, DateAdded, StockNumber, GenreId) VALUES (1, 'Hangover', 04/12/2015, 10/16/2015, 5, 1)");
            Sql("INSERT INTO Movies (Id, Name, ReleaseDate, DateAdded, StockNumber, GenreId) VALUES (2, 'Die Hard', '03/18/2000', '09/11/2000', 3, 2)");
            Sql("INSERT INTO Movies (Id, Name, ReleaseDate, DateAdded, StockNumber, GenreId) VALUES (3, 'The Terminator', '02/14/2006', '08/16/2006', 6, 2)");
            Sql("INSERT INTO Movies (Id, Name, ReleaseDate, DateAdded, StockNumber, GenreId) VALUES (4, 'Toy Story', '01/17/1999', '06/16/1999', 2, 3)");
            Sql("INSERT INTO Movies (Id, Name, ReleaseDate, DateAdded, StockNumber, GenreId) VALUES (5, 'Titanic', '10/16/2001', '02/19/2002', 4, 4)");
            Sql("SET IDENTITY_INSERT Movies OFF");
        }
        
        public override void Down()
        {
        }
    }
}
