namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'23eda275-7325-4c29-bedc-e56ef8458e2a', N'dino.mrkonjic@gmail.com', 0, N'AAupNcVjjuTyXOJ6GLy5iMPZmrDWNVfh9M4SuLrDmnw4xTSsuZO8RfFcK+MbexMLTQ==', N'92c78c0e-f3a2-47d9-937e-27d1cb6c8282', NULL, 0, 0, NULL, 1, 0, N'dino.mrkonjic@gmail.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c44002da-bf39-433a-98f8-6bfb97e9ebcf', N'admin@vidly.com', 0, N'AJ/VVUc1/MIUcjJHE8StfARRKiug/mtbBdRsjXUrXlzQYk2k05VgnGTORsezJOvMAg==', N'ca185358-5b2e-4980-b4db-fa19f4234078', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd5bdd0f1-7355-4515-9b75-fbb14f783537', N'CanManageMovies')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c44002da-bf39-433a-98f8-6bfb97e9ebcf', N'd5bdd0f1-7355-4515-9b75-fbb14f783537')
");
        }
        
        public override void Down()
        {
        }
    }
}
