namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateCustomers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Customers (Name, IsSubscribedToNewsletter, MembershipTypeId, Birthday) VALUES ('Mary Jane', 1, 2, '10/15/1989')");
            Sql("INSERT INTO Customers (Name, IsSubscribedToNewsletter, MembershipTypeId, Birthday) VALUES ('John May', 0, 3, '03/15/1989')");
            Sql("INSERT INTO Customers (Name, IsSubscribedToNewsletter, MembershipTypeId, Birthday) VALUES ('Chris Rea', 1, 4, '11/12/1990')");
            Sql("INSERT INTO Customers (Name, IsSubscribedToNewsletter, MembershipTypeId, Birthday) VALUES ('Marco Polo', 1, 1, '12/21/1980')");
        }
        
        public override void Down()
        {
        }
    }
}
