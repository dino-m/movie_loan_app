#Movie loan app

ASP.NET MVC 5 application for renting movies, created using Entity Framework.

###Technologies used:
- ASP.NET MVC
- Entity Framework
- Javascript
- jQuery
- Ajax

###DESCRIPTION:

- Use cases:
    - Register and login user
    - Login via facebook
    - Customers CRUD with sorting, filtering and pagination
    - Movies CRUD with sorting, filtering and pagination
    - Creating Rental add customer who is loaning several movies from database which are available (NOT out of stock).
    - User privileges (admin and guest) - Admin has all privileges and can do all operations, Guest can only read customers and movies

- Rental option has auto completion (Twitter Typeahead)
- App has some migrations for customers, movies and users
- Views are returned with View Models
- Mapping is is using AutoMapper